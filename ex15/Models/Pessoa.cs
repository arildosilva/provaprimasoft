using System;

namespace ex15.Models
{
    abstract class Pessoa
    {
        public int Codigo { get; set; }
        
        public String Nome { get; set; }
    }
}
