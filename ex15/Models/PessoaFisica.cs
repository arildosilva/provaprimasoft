using System;

namespace ex15.Models
{
    class PessoaFisica : Pessoa
    {
        public String CPF { get; set; }
    }
}
