﻿using System;
using ex15.Models;

namespace ex15
{
    class Program
    {
        static void Main(string[] args)
        {
            PessoaFisica pf = new PessoaFisica {Codigo = 2, Nome = "Arildo", CPF = "123.456.789-10"};
            Console.WriteLine(String.Format("PessoaFisica: [{0}][{1}][{2}]", pf.Codigo, pf.Nome, pf.CPF));
        }
    }
}
