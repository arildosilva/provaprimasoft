using System;

namespace ex16.Utils
{
    class Comparavel
    {
        int valorInicial { get; set; }

        public Comparavel(int valorInicial)
        {
            this.valorInicial = valorInicial;
        }

        public bool MaiorOuIgual(int parametro) => this.valorInicial >= parametro;

        public bool MenorOuIgual(int parametro) => this.valorInicial <= parametro;

        public bool DiferenteDe(int parametro) => this.valorInicial != parametro;
    }
}
