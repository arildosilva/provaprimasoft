﻿using System;
using ex16.Utils;

namespace ex16
{
    class Program
    {
        static void Main(string[] args)
        {
            Comparavel comparavel = new Comparavel(10);

            Console.WriteLine("DiferenteDe");
            Console.WriteLine(comparavel.DiferenteDe(10));
            Console.WriteLine(comparavel.DiferenteDe(1));

            Console.WriteLine("MaiorOuIgual");
            Console.WriteLine(comparavel.MaiorOuIgual(10));
            Console.WriteLine(comparavel.MaiorOuIgual(20));
            Console.WriteLine(comparavel.MaiorOuIgual(0));

            Console.WriteLine("MenorOuIgual");
            Console.WriteLine(comparavel.MenorOuIgual(10));
            Console.WriteLine(comparavel.MenorOuIgual(20));
            Console.WriteLine(comparavel.MenorOuIgual(0));
        }
    }
}
