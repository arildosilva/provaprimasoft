using System;

namespace ex17.Utils
{
	class CalculadoraRefatorada
	{
		private int PrimeiroNumero;
        private int SegundoNumero;
		private char Operacao;

		public CalculadoraRefatorada(int PrimeiroNumero, int SegundoNumero, char Operacao)
		{
			this.PrimeiroNumero = PrimeiroNumero;
            this.SegundoNumero = SegundoNumero;
            this.Operacao = Operacao;
		}

		int RealizarOperacao()
		{
            switch (Operacao)
            {
                case '+':
                    return PrimeiroNumero + SegundoNumero;
                case '-':
                    return PrimeiroNumero - SegundoNumero;
                case '/':
                    try
                    {
                        return PrimeiroNumero / SegundoNumero;
                    }
                    catch (DivideByZeroException e)
                    {
                        Console.WriteLine("Erro na divisao", e);
                        return 0;
                    }
                default:
                    return PrimeiroNumero * SegundoNumero;
            }
		}
	}
}
