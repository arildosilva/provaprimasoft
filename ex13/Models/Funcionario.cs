using System;
using System.ComponentModel.DataAnnotations;

namespace ex13.Models
{
    class Funcionario
    {
        [Required]
        public String Nome { get; set; }

        [Required]
        public DateTime DataNascimento { get; set; }

        [Required]
        public DateTime DataContratacao { get; set; }

        public int Idade { get; set; }

        public int TempoDeTrabalho { get; set; }

        public bool AptoParaAposentar { get; set; }

        public override string ToString()
        {
            return String.Format("Funcionario [{0}]: Idade {1} e Tempo de trabalho {2} [{3}]", Nome, Idade, TempoDeTrabalho, AptoParaAposentar ? "Apto para aposentadoria" : "Inapto para aposentadoria");
        }
    }
}
