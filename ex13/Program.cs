﻿using System;
using System.Globalization;
using System.Linq;
using System.Reflection;
using ex13.Models;

namespace ex13
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Exercicio 13");

            Console.WriteLine("Digite o nome do funcionario");
            String NomeInput = Console.ReadLine();
            Console.WriteLine("Digite a data de nascimento (dd/mm/yyyy)");
            String DataNascimentoInput = Console.ReadLine();
            Console.WriteLine("Digite a data de contratacao (dd/mm/yyyy)");
            String DataContratacaoInput = Console.ReadLine();

            Funcionario funcionario = null;

            try
            {
                funcionario = new Funcionario {
                    Nome = NomeInput,
                    DataNascimento = DateTime.ParseExact(DataNascimentoInput, "dd/MM/yyyy", CultureInfo.InvariantCulture),
                    DataContratacao = DateTime.ParseExact(DataContratacaoInput, "dd/MM/yyyy", CultureInfo.InvariantCulture)
                };

                funcionario.Idade = diferencaDeDatas(funcionario.DataNascimento);
                funcionario.TempoDeTrabalho = diferencaDeDatas(funcionario.DataContratacao);
            }
            catch (ArgumentNullException e)
            {
                Console.WriteLine("Nenhuma data informada.", e);
            }
            catch (FormatException e)
            {
                Console.WriteLine("Formato de data inválido.", e);
            }

            if (funcionario != null)
            {
                validaFuncionario(funcionario);

                Console.WriteLine(funcionario.ToString());
            }
            else
            {
                Console.WriteLine("Erro no cadastro do funcionario!");
            }
        }

        static int diferencaDeDatas(DateTime DataDeEntrada)
        {
            if (DataDeEntrada == null)
            {
                return 0;
            }

            return (int)Math.Floor((DateTime.Now - DataDeEntrada).TotalDays / 365.25D);
        }

        static void validaFuncionario(Funcionario funcionario)
        {
            String namespaceValidacoes = "ex13.RegraAposentadoria";

            var regras = from tipos in Assembly.GetExecutingAssembly().GetTypes()
                where tipos.IsClass && tipos.Namespace == namespaceValidacoes
                select tipos;
            
            foreach (Type regra in regras.ToList())
            {
                MethodInfo metodoValidacao = regra.GetMethod("validaFuncionario");

                if (metodoValidacao != null)
                {
                    // Console.WriteLine(String.Format("Executando validacao {0}", regra.Name));
                    
                    object instancia = Activator.CreateInstance(regra, null);
                    metodoValidacao.Invoke(instancia, new object[] { funcionario } );
                }
            }
        }
    }
}
