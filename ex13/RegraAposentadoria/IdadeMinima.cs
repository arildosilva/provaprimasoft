using System;
using ex13.Models;

namespace ex13.RegraAposentadoria
{
    class IdadeMinima
    {
        public void validaFuncionario(Funcionario funcionario)
        {
            if (!funcionario.AptoParaAposentar)
            {
                funcionario.AptoParaAposentar = funcionario.Idade >= 65;
            }
        }
    }
}
