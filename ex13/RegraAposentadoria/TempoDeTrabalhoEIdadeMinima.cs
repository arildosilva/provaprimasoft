using System;
using ex13.Models;

namespace ex13.RegraAposentadoria
{
    class TempoDeTrabalhoEIdadeMinima
    {
        public void validaFuncionario(Funcionario funcionario)
        {
            if (!funcionario.AptoParaAposentar)
            {
                funcionario.AptoParaAposentar = funcionario.TempoDeTrabalho >= 25 && funcionario.Idade >= 60;
            }
        }
    }
}
