﻿using System;

namespace ex11
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Escreva um algoritmo, para mostrar na tela os números ímpares entre 100 e 1000 em ordem inversa (999, 997, ..., 101).");

            for (int i = 1000; i > 100; i--)
            {
                if (i % 2 == 1)
                {
                    Console.WriteLine(i);
                }
            }
        }
    }
}
