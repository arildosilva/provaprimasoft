﻿using System;

namespace ex14
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Digite o número total de mercadorias no estoque");
            String TotalDeMercadoriasInput = Console.ReadLine();
            int TotalDeMercadorias = 0;

            if (Int32.TryParse(TotalDeMercadoriasInput, out TotalDeMercadorias) && TotalDeMercadorias > 0)
            {
                double ValorTotal = ProcessarMercadorias(TotalDeMercadorias);
                double MediaDeValor = CalcularMediaDeValor(ValorTotal, TotalDeMercadorias);

                Console.WriteLine(String.Format("O valor total das mercadorias é de {0:0.00} e a média de valor é de {1:0.00}", ValorTotal, MediaDeValor));
            }
            else
            {
                Console.WriteLine("Entrada inválida");
            }
        }

        static double ProcessarMercadorias(int TotalDeMercadorias)
        {
            double ValorTotal = 0;
            
            for (int i = 1; i <= TotalDeMercadorias; i++)
            {
                Console.WriteLine(String.Format("Digite o valor da mercadoria {0}", i));
                String ValorDaMercadoriaInput = Console.ReadLine();
                double ValorDaMercadoria = 0;

                if (Double.TryParse(ValorDaMercadoriaInput.Replace(',', '.'), out ValorDaMercadoria))
                {
                    ValorTotal += ValorDaMercadoria;
                }
                else
                {
                    Console.WriteLine("Entrada inválida, digite novamente, por favor");
                    i--;
                }
            }

            return ValorTotal;
        }

        static double CalcularMediaDeValor(double ValorTotal, int TotalDeMercadorias)
        {
            if (TotalDeMercadorias > 0)
            {
                return ValorTotal / TotalDeMercadorias;
            }

            return 0;
        }
    }
}
