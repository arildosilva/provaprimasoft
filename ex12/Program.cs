﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ex12
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Escreva um algoritmo que receba uma lista de 100 números e, no final, apresente o maior e o menor entre eles.");

            List<int> numeros = new List<int>();

            Console.WriteLine("Digite a lista de numeros separados por ; (1;2;3) ou aperte Enter para testar com numeros aleatorios.");

            String resposta = Console.ReadLine();

            if (string.IsNullOrEmpty(resposta))
            {
                int count = 100;
                Random numeroAleatorio = new Random();
                
                for (int i = 0; i < count; i++)
                {
                    numeros.Add(numeroAleatorio.Next(1, 10000));
                }
            } else {
                int numResposta;
                foreach (String item in resposta.Split(";"))
                {
                    if(Int32.TryParse(item, out numResposta))
                    {
                        numeros.Add(numResposta);
                    }
                }
            }
            
            apresentaMinEMax(numeros);
        }

        private static void apresentaMinEMax(List<int> numeros)
        {
            if (numeros != null && numeros.Any())
            {
                Console.WriteLine(String.Format("Minimo: {0} / Maximo: {1}", numeros.Min(), numeros.Max()));
            }
            else
            {
                Console.WriteLine("A lista está vazia!");                
            }
        }
    }
}
